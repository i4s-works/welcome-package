# Day-1 survival guide

Many things may seem confusing at first, especially in a team that is split between two sites and two universities. Here is what you should know and do when settling on our shores.

## Common

* [ ] **I4S mailing lists:** You can either send an e-mail to [thibaud.toullier@univ-eiffel.fr](mailto:thibaud.toullier@univ-eiffel.fr) and/or [romain.noel@univ-eiffel.fr](romain.noel@univ-eiffel.fr) with your name and status (PhD, post-doc, intern...), or add an item with your name, e-mail address and status in [the TODO section of this shared note](https://notes.inria.fr/527bKOt2RTaCQXUaSzO10g#). You shall be added to the appropriate mailing lists in the following days and granted access to (parts of) the team's GitLab group.
* [ ] **I4S-dev mailing list:** If you are a non-permanent team member and will be writing code at any point, you may send an e-mail to [mathias.malandain@inria.fr](mathias.malandain@inria.fr) to be added to the I4S-dev mailing list. This way, you will be invited to recurrent developer meetings, be informed about all training courses by Inria software engineers, be able to ask for help with programming/software/development practices whenever needed, and more.

**TODO**

## Specific to Inria

* [ ] **Hardware and accessories:** You should be provided with a docking station, an external disk drive (for automatic file backups) and a privacy filter for your screen (for laptops). If this is not the case, open a ticket on Inria's Helpdesk to ask for them. The external disk drive should remain connected to your docking station, and your laptop (if you have one) should be plugged to the docking station every time you are in your office.
* [ ] **VPN:** Check that the Cisco AnyConnect client is installed on your computer. If it is not, check out [the dedicated page on Doc-SI](https://doc-si.inria.fr/display/SU/VPN#) to install and use it. *There are many things that cannot be done outside of the Inria premises without the VPN on.*
* [ ] **Physical backups:** Automatic data backups on a hard disk drive have to be set up. Tools that are already installed on your machine can help, such as [Backup](https://www.windowscentral.com/how-backup-windows-10-automatically) for Windows or [Time Machine](https://support.apple.com/en-us/HT201250) for Mac OS. On Ubuntu 22.04, [Deja Dup](https://tecadmin.net/backup-and-restore-ubuntu-system/) is a simple solution.
* [ ] **Network backups:** Your home or user folder should automatically be backed up on the Inria secure servers by the Lina Atempo tool. Open the client and check that backups are planned. If the client is not installed or backups were not set up, go to [the dedicated page on Doc-SI](https://doc-si.inria.fr/display/SU/Doc+Sauvegarde+Atempo#) and follow instructions for your OS.
* [ ] **A few links you have to know:** There are a lot more, that are detailed in another note, but here are the few ones that you should know from Day 1. They should already be bookmarked in your browser, along with a lot more that you will not be clicking on very often.
  * [ ] *E-mail + calendar:* [zimbra.inria.fr](zimbra.inria.fr)
  * [ ] *Intranet:* [intranet.inria.fr](zimbra.inria.fr) (a few useful features there: a search menu that also works for finding the office number of a colleague, and a sidebar that provides you with shortcuts to some useful tools and platforms)
  * [ ] *Doc SI:* [doc-si.inria.fr](doc-si.inria.fr) (where you should go as soon as you have difficulties with installing/using software)
  * [ ] *Helpdesk:* [helpdesk.inria.fr](helpdesk.inria.fr) (where you can go if the previous step failed, or if your local toilets are out of soap, or if you would like to get some equipment, or lend a HDD, or... anything, really)

**TODO**

## Specific to UGE

**TODO**