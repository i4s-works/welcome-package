# Welcome Package

Hello and welcome! Here are a few resources that may help you for your first days/weeks in our team.

Please do not hesitate to start with our [Day 1 cheatsheet](Day1.md). Once every box is checked there, we hope you enjoy this more complete list of resources, tools and platforms!

## Research practices

* The [Passport For Open Science](https://www.ouvrirlascience.fr/passeport-pour-la-science-ouverte-guide-pratique-a-lusage-des-doctorants/) is a pretty handy guide, with a lot of (open) tools and good practices for delving into a state-of-the-art, managing your bibliography, disseminating your work, and so on. Highly recommended!