# Week-1 notebook

If you already went through our [Day-1 survival guide](./Day1.md), congratulations! There are still a lot of things that may help you in your journey, from tools to websites to neat tricks. Let's do this.

## Communication channels

We are using quite a lot of communication tools and channels. Here are the main ones.

### Common

* **Mailing lists:** There are four aliases you have to know.
  * *All permanent members:* [i4s.permanents@listes.univ-eiffel.fr](mailto:i4s.permanents@listes.univ-eiffel.fr)
  * *All non-permanent members:* [i4s.non-permanents@listes.univ-eiffel.fr](mailto:i4s.non-permanents@listes.univ-eiffel.fr)
  * *Team assistants:* [i4s.administratif@listes.univ-eiffel.fr](i4s.administratif@listes.univ-eiffel.fr)
  * *Everyone at once:* [i4s.personnels@listes.univ-eiffel.fr](mailto:i4s.personnels@listes.univ-eiffel.fr)

**TODO**

### Specific to Inria

* **Mattermost servers:** Inria hosts its own Mattermost instance and several very useful servers were created there. Here are a few:
  * *National Inria server:* [https://mattermost.inria.fr/inria](https://mattermost.inria.fr/inria)
  * *Inria Rennes server:* [https://mattermost.inria.fr/inria-rba--irisa](https://mattermost.inria.fr/inria-rba--irisa)
  * *National development server:* [https://mattermost.inria.fr/devel](https://mattermost.inria.fr/devel) (a lot of very useful information about languages and tools, and a pretty responsive community)
  * *National AGOS server:* [https://mattermost.inria.fr/agos](https://mattermost.inria.fr/agos) (for all the news about our beloved association)
  * *AGOS Rennes server:* [https://mattermost.inria.fr/cgl-rba](https://mattermost.inria.fr/cgl-rba) (for local annoucements, suggestions, events...)

**TODO**

### Specific to UGE

**TODO**

## Resources

### Common

**TODO**

### Specific to Inria

**TODO**

### Specific to UGE

**TODO**

## Going further

**LINKS**